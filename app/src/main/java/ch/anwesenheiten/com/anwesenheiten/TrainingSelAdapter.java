package ch.anwesenheiten.com.anwesenheiten;

/**
 * Created by georg on 02.03.18.
 */


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;


public class TrainingSelAdapter extends ArrayAdapter<Training> {

    private Context context;
    private List<Training> items;
    private final int[] bgColors = new int[] { R.color.list_bg_1, R.color.list_bg_2 };
    Training training;

    public TrainingSelAdapter(Context cont, int textViewResourceId, List<Training> items)  {

        super(cont, textViewResourceId, items);
        this.context = cont;
        this.items = items;

    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View element = inflater.inflate(R.layout.rowlayout, null);

        int colorPosition = position % 2;
        element.setBackgroundResource(bgColors[colorPosition]);

        TextView label1 = (TextView) element.findViewById(R.id.label1);
        TextView label2 = (TextView) element.findViewById(R.id.label2);
        TextView label3 = (TextView) element.findViewById(R.id.label3);


        training = items.get(position);

        label1.setText(training._T_wotag);
        label2.setText(training._T_startzeit);
        label3.setText(training._T_endzeit);

        return element;
    }




}