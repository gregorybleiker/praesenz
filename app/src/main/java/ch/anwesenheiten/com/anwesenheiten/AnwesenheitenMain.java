package ch.anwesenheiten.com.anwesenheiten;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.lang.Object;

public class AnwesenheitenMain extends Activity  {

    Button buttons[] = new Button[24];
    public int[] prefindex = new int[24];
    AnwesenheitenDBHandler database ;
    SQLiteDatabase connection ;
    private Button seltrainbutton;
    private Button personlistbutton;
    private TextView trainingfield;
    List<Training> 		values;
    List<Person> pvalues;
    int vsize;
    private int selectedtraining;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Initialize fields

        for (int i = 0; i < prefindex.length; i++) {
            prefindex[i] = 0;
        }

        seltrainbutton = (Button)findViewById(R.id.Buttonseltrain);
        personlistbutton = (Button)findViewById(R.id.personlistbutton);
        trainingfield = (TextView) findViewById(R.id.Training);

        seltrainbutton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                showtrainselection();

            }
        });

        personlistbutton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                showpersonlist();

            }
        });

        for (int i=0; i < 24; i++)  {
            String buttonID = "button" + i;
            int resID = getResources().getIdentifier(buttonID, "id",
                    "ch.anwesenheiten.com.anwesenheiten");
            buttons[i] = (Button) findViewById(resID);

        }

        //temporary value to be replaced with real training number
        int trainid = selectedtraining;

        database = new AnwesenheitenDBHandler(this);
        connection = database.getWritableDatabase();

        List<Candidate> clist;
        Candidate ca;

        clist = database.getAllCandidates();

        int clnbr = clist.size();
        int count = 0;
        int butindex=1;


        while (count < clnbr) {

            ca = clist.get(count);
            String stref = ca.gettref();
            int tref = Integer.parseInt(stref);


            if (tref ==  trainid) {

                String pref = ca._C_pref;
                int pers = Integer.parseInt(pref);

                // store pers ref in screen index
                if (pers != 0) {
                    prefindex[butindex - 1] = pers;

                    String picref = getpicref(pers);
                    String piref = "people_mini.png";
                    if (piref.equals(picref)) {
                        buttons[butindex].setBackground(getResources().getDrawable(R.drawable.people_mini));

                    }
                    else
                    {
                        LayerDrawable ld = drawlayers_plain(picref);

                        buttons[butindex].setBackground(ld);

                    }
                }
                butindex++;

            }
            count ++;
        }

        int i= 0;

        for (i = 0; i < 24; i++) {
            int butid = i+1;
            String buttonID = "button" + butid;
            int resID = getResources().getIdentifier(buttonID, "id",
                    "ch.anwesenheiten.com.anwesenheiten");
            buttons[i] = (Button) findViewById(resID);
            buttons[i].setOnLongClickListener(new View.OnLongClickListener() {

                @Override
                public boolean onLongClick(View v) {
                    //get Id of button
                    int gpbn= getlongpressedbuttnbr(v);
                    //get Persid from index
                    int pers = prefindex[gpbn-1];
                    if (pers != 0) {
                        String picref = getpicref(pers);

                        if (v.isSelected()) {
                            v.setSelected(false);
                            LayerDrawable ld = drawlayers_plain(picref);
                            v.setBackground(ld);

                        } else {
                            v.setSelected(true);
                            LayerDrawable ld = drawlayers_bordered(picref);
                            v.setBackground(ld);
                        }
                    }
                    return true;
                }

            });
        }



    }

    public void pressbutton(View v) {

        int i = getpref(v);

        modperson(i);
    }

    public void modperson(int i) {

        Intent intent = new Intent(this, ModPerson.class);
        intent.putExtra("nbrpers", i);
        startActivity(intent);
    }

    public void training(View v) {

        Intent intent = new Intent(this, ModTraining.class);
        startActivity(intent);
    }

    public void save(View v) {

        String s_tref = Integer.toString(selectedtraining);
        String s_pref = null;
        String s_date= null;
        String s_trainingfield;

        for (int i=0; i < 24; i++)  {
            int ibut = i+1;
            String buttonID = "button" + ibut;
            int resID = getResources().getIdentifier(buttonID, "id",
                    "ch.anwesenheiten.com.anwesenheiten");
            buttons[i] = (Button) findViewById(resID);

            if (buttons[i].isSelected()) {

                s_pref = Integer.toString(prefindex[i]);

                Calendar c = Calendar.getInstance();
                Date date = new Date();
                s_date = date.toString();
                c.setTime(date);
                int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
                s_trainingfield = trainingfield.getText().toString();

                Participant part= new Participant(s_trainingfield, s_date, s_tref, s_pref);
                int iret = (int) database.addparticipants(part);

                Toast.makeText(getApplicationContext(),
                        "Participants saved in database",
                        Toast.LENGTH_LONG).show();

            }

        }

    }

    public void export(View v) {
        String s = "anwdb";
        Context c = getApplicationContext();
        exportdb( c, s );

        Toast.makeText(this, "DB exported", Toast.LENGTH_SHORT).show();;
    }

    public static void exportdb(Context c, String dbname) {
        String databasePath = c.getDatabasePath(dbname).getPath();
        File f = new File(databasePath);
        OutputStream myOutput = null;
        InputStream myInput = null;
        Log.d("testing", " testing db path " + databasePath);
        Log.d("testing", " testing db exist " + f.exists());

        if (f.exists()) {
            try {

                String pathToExternalStorage = Environment.getExternalStorageDirectory().toString();
                File directory = new File(pathToExternalStorage + "/" + "Anwesenheiten");

 //              File directory = new File("/mnt/sdcard/DB_DEBUG");
                if (!directory.exists())
                    directory.mkdir();

                myOutput = new FileOutputStream(directory.getAbsolutePath()
                        + "/" + dbname);
                myInput = new FileInputStream(databasePath);

                byte[] buffer = new byte[1024];
                int length;
                while ((length = myInput.read(buffer)) > 0) {
                    myOutput.write(buffer, 0, length);
                }

                myOutput.flush();
            } catch (Exception e) {
            } finally {
                try {
                    if (myOutput != null) {
                        myOutput.close();
                        myOutput = null;
                    }
                    if (myInput != null) {
                        myInput.close();
                        myInput = null;
                    }
                } catch (Exception e) {
                }
            }
        }
    }

    public LayerDrawable drawlayers_plain(String spic) {

        String spi = "people_mini.png";
        Drawable dpic;
        if (spi.equals(spic)) {
//            dpic = getResources().getDrawable(R.drawable.people_mini);
            Drawable d = getResources().getDrawable(R.drawable.people_mini);
            Bitmap bitmap = ((BitmapDrawable) d).getBitmap();
            dpic = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, 100, 100, true));
        }
        else {
            Bitmap bpic = BitmapFactory.decodeFile(spic);
            Matrix matrix = new Matrix();
            matrix.preRotate(00);
            Bitmap rotatedBitmap = Bitmap.createBitmap(bpic, 0, 0, bpic.getWidth(), bpic.getHeight(), matrix, true);
            BitmapDrawable bdrawable = new BitmapDrawable(rotatedBitmap.createScaledBitmap(rotatedBitmap, 100, 100, true));

            dpic = bdrawable;
  //          dpic = new BitmapDrawable(bpic);
        }
        Drawable[] layers = new Drawable[]{
                dpic
        };

        LayerDrawable layerDrawable = new LayerDrawable(layers);

        // draw pic
        layerDrawable.setLayerInset(0,0,0,0,0);


        return layerDrawable;

    }

    public LayerDrawable drawlayers_bordered(String spic) {

        ColorDrawable leftBorder = new ColorDrawable(Color.GREEN);
        ColorDrawable topBorder = new ColorDrawable(Color.GREEN);
        ColorDrawable rightBorder = new ColorDrawable(Color.GREEN);
        ColorDrawable bottomBorder = new ColorDrawable(Color.GREEN);
        ColorDrawable background = new ColorDrawable(Color.WHITE);

        String spi = "people_mini.png";
        Drawable dpic;
        if (spi.equals(spic)) {
            Drawable d = getResources().getDrawable(R.drawable.people_mini);
            Bitmap bitmap = ((BitmapDrawable) d).getBitmap();
            dpic = new BitmapDrawable(getResources(), Bitmap.createScaledBitmap(bitmap, 100, 100, true));
        }
        else {

            Bitmap bpic = BitmapFactory.decodeFile(spic);
            Matrix matrix = new Matrix();
            matrix.preRotate(00);
            Bitmap rotatedBitmap = Bitmap.createBitmap(bpic, 0, 0, bpic.getWidth(), bpic.getHeight(), matrix, true);
            BitmapDrawable bdrawable = new BitmapDrawable(rotatedBitmap.createScaledBitmap(rotatedBitmap, 100, 100, true));

            dpic = bdrawable;
//            dpic = new BitmapDrawable(bpic);
        }

        Drawable[] layers = new Drawable[]{
                leftBorder,
                topBorder,
                rightBorder,
                bottomBorder,
                dpic
        };

        LayerDrawable layerDrawable = new LayerDrawable(layers);

        // draw left border
        layerDrawable.setLayerInset(0,0,0,15,0);
        // draw top border
        layerDrawable.setLayerInset(1,15,0,0,15);
        //  draw right border
        layerDrawable.setLayerInset(2,15,15,0,0);
        // draw bottom border
        layerDrawable.setLayerInset(3,15,15,15,0);
        // draw pic
        layerDrawable.setLayerInset(4,15,15,15,15);


        return layerDrawable;

    }

    public String getpicref(int pers) {

        database = new AnwesenheitenDBHandler(this);
        connection = database.getWritableDatabase();

        Person p = database.opensingleperson(pers, connection);
        String spic = p._T_pic;

        return spic;

    }

    public int getpref(View v)     {
        int i = 0;
        switch (v.getId())  //get the id of the view clicked. (in this case button)
        {
            case R.id.button1: // if its button1
                i= prefindex[0];
                break;
            case R.id.button2: // if its button2
                i= prefindex[1];
                break;
            case R.id.button3: // if its button3
                i= prefindex[2];
                break;
            case R.id.button4: // if its button4
                i= prefindex[3];
                break;
            case R.id.button5: // if its button5
                i= prefindex[4];
                break;
            case R.id.button6: // if its button6
                i= prefindex[5];
                break;
            case R.id.button7: // if its button7
                i= prefindex[6];
                break;
            case R.id.button8: // if its button8
                i= prefindex[7];
                break;
            case R.id.button9: // if its button9
                i= prefindex[8];
                break;
            case R.id.button10: // if its button10
                i= prefindex[9];
                break;
            case R.id.button11: // if its button11
                i= prefindex[10];
                break;
            case R.id.button12: // if its button12
                i= prefindex[11];
                break;
            case R.id.button13: // if its button13
                i= prefindex[12];
                break;
            case R.id.button14: // if its button14
                i= prefindex[13];
                break;
            case R.id.button15: // if its button15
                i= prefindex[14];
                break;
            case R.id.button16: // if its button16
                i= prefindex[15];
                break;
            case R.id.button17: // if its button17
                i= prefindex[16];
                break;
            case R.id.button18: // if its button18
                i= prefindex[17];
                break;
            case R.id.button19: // if its button19
                i= prefindex[18];
                break;
            case R.id.button20: // if its button20
                i= prefindex[19];
                break;
            case R.id.button21: // if its button21
                i= prefindex[20];
                break;
            case R.id.button22: // if its button22
                i= prefindex[21];
                break;
            case R.id.button23: // if its button23
                i= prefindex[22];
                break;
            case R.id.button24: // if its button24
                i= prefindex[23];
                break;
        }

        return i;
    }

    public int getlongpressedbuttnbr(View v)     {
        int i = 0;
        switch (v.getId())  //get the id of the view clicked. (in this case button)
        {
            case R.id.button1: // if its button1
                i= 1;
                break;
            case R.id.button2: // if its button2
                i= 2;
                break;
            case R.id.button3: // if its button3
                i= 3;
                break;
            case R.id.button4: // if its button4
                i= 4;
                break;
            case R.id.button5: // if its button5
                i= 5;
                break;
            case R.id.button6: // if its button6
                i= 6;
                break;
            case R.id.button7: // if its button7
                i= 7;
                break;
            case R.id.button8: // if its button8
                i= 8;
                break;
            case R.id.button9: // if its button9
                i= 9;
                break;
            case R.id.button10: // if its button10
                i= 10;
                break;
            case R.id.button11: // if its button11
                i= 11;
                break;
            case R.id.button12: // if its button12
                i= 12;
                break;
            case R.id.button13: // if its button13
                i= 13;
                break;
            case R.id.button14: // if its button14
                i= 14;
                break;
            case R.id.button15: // if its button15
                i= 15;
                break;
            case R.id.button16: // if its button16
                i= 16;
                break;
            case R.id.button17: // if its button17
                i= 17;
                break;
            case R.id.button18: // if its button18
                i= 18;
                break;
            case R.id.button19: // if its button19
                i= 19;
                break;
            case R.id.button20: // if its button20
                i= 20;
                break;
            case R.id.button21: // if its button21
                i= 21;
                break;
            case R.id.button22: // if its button22
                i= 22;
                break;
            case R.id.button23: // if its button23
                i= 23;
                break;
            case R.id.button24: // if its button24
                i= 24;
                break;
        }

        return i;
    }

    void showtrainselection() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        String trainlist = "Trainings";
        String trainlistempty = "no Trainings available";

        int checkedItem = 0;
        AnwesenheitenDBHandler database = new AnwesenheitenDBHandler(this);
        values = database.getAllTrainings();

        if (values.isEmpty())
        {
            builder.setTitle(trainlistempty);
        }
        else
        {

            builder.setTitle(trainlist);
            vsize = values.size();

            TrainingSelAdapter adapter = new TrainingSelAdapter(this,R.layout.rowlayout, values);

            builder.setSingleChoiceItems(adapter, checkedItem, new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int item) {

                    int selectedPosition = ((AlertDialog)dialog).getListView().getCheckedItemPosition();
                    Training selectedItem = (Training) ((AlertDialog)dialog).getListView().getItemAtPosition(selectedPosition);
                    setfields(selectedItem.getID(), selectedItem.getwotag(), selectedItem.getstartzeit(), selectedItem.getendzeit());
                    dialog.dismiss();
                }

            });

        }
        builder.show();

    };

    void setfields(int seltrainid, String wotag, String szeit, String ezeit) {

       selectedtraining = seltrainid;

        wotag = wotag+" "+szeit+" "+ezeit;
        onCreate(new Bundle());
        trainingfield.setText(wotag);
        trainingfield.setTextSize(20);
    };

    public void showpersonlist() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        String plist = "Person List";
        String plistempty = "no Persons available";

        int checkedItem = 0;
        AnwesenheitenDBHandler database = new AnwesenheitenDBHandler(this);
        pvalues = database.getAllPerson();

        if (pvalues.isEmpty())
        {
            builder.setTitle(plistempty);
        }
        else
        {

            builder.setTitle(plist);
            vsize = pvalues.size();

            PersonSelAdapter adapter = new PersonSelAdapter(this,R.layout.rowlayout, pvalues);

            builder.setSingleChoiceItems(adapter, checkedItem, new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int item) {

                    int selectedPosition = ((AlertDialog)dialog).getListView().getCheckedItemPosition();
                    Person selectedItem = (Person) ((AlertDialog)dialog).getListView().getItemAtPosition(selectedPosition);
                    modperson(selectedItem.getID());
                    dialog.dismiss();
                }

            });

        }

        builder.show();

    };

}























