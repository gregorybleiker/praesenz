package ch.anwesenheiten.com.anwesenheiten;

/**
 * Created by georg on 02.03.18.
 */


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;


public class PersonSelAdapter extends ArrayAdapter<Person> {

    private Context context;
    private List<Person> items;
    private final int[] bgColors = new int[] { R.color.list_bg_1, R.color.list_bg_2 };
    Person person;

    public PersonSelAdapter(Context cont, int textViewResourceId, List<Person> items)  {

        super(cont, textViewResourceId, items);
        this.context = cont;
        this.items = items;

    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View element = inflater.inflate(R.layout.rowlayout, null);

        int colorPosition = position % 2;
        element.setBackgroundResource(bgColors[colorPosition]);

        TextView label1 = (TextView) element.findViewById(R.id.label1);
        TextView label2 = (TextView) element.findViewById(R.id.label2);
        TextView label3 = (TextView) element.findViewById(R.id.label3);


        person = items.get(position);

        label1.setText(person._T_vname);
        label2.setText(person._T_nname);
        label3.setText(person._T_ort);

        return element;
    }




}