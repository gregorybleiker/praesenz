package ch.anwesenheiten.com.anwesenheiten;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class AnwesenheitenDBHandler extends SQLiteOpenHelper {
    private static int DB_VERSION = 1;
    private Context context;
    private static final String KEY_PID = "id";
    private static final String KEY_VNAME = "vorname";
    private static final String KEY_NNAME = "nachname";
    private static final String KEY_ADR = "adresse";
    private static final String KEY_PLZ = "plz";
    private static final String KEY_ORT = "ort";
    private static final String KEY_GEB = "geburt";
    private static final String KEY_TEL = "telnr";
    private static final String KEY_PIC  = "picref";

    private static final String KEY_TID = "id";
    private static final String KEY_WOTAG = "wotag";
    private static final String KEY_STIME = "startzeit";
    private static final String KEY_ETIME = "endzeit";

    private static final String KEY_TTID = "id";
    private static final String KEY_TTRAIN = "train";
    private static final String KEY_DAT = "datum";
    private static final String KEY_TREF = "tref";
    private static final String KEY_PREF = "pref";

    private static final String KEY_CID = "id";
    private static final String KEY_CTRA = "tref";
    private static final String KEY_CPER = "pref";

    private static final String TABLE_PERS = "person";
    private static final String TABLE_TRAIN = "training";
    private static final String TABLE_PART = "participants";
    private static final String TABLE_CAND = "candidate";

    private String[] allColumns = {KEY_PID, KEY_VNAME, KEY_NNAME, KEY_ADR, KEY_PLZ, KEY_ORT,
            KEY_GEB, KEY_TEL, KEY_PIC};
    private String[] allColumns2 = {KEY_TID, KEY_WOTAG, KEY_STIME, KEY_ETIME};
    private String[] allColumns3 = {KEY_TTID, KEY_TTRAIN, KEY_DAT, KEY_PREF, KEY_TREF};
    private String[] allcolumns4 = {KEY_CID, KEY_CTRA, KEY_CPER};


    private String[] array = new String[36];

    AnwesenheitenDBHandler(Context context) {
        super(
                context,
                context.getResources().getString(R.string.dbname),
                null,
                DB_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        for (String sql : context.getResources().getStringArray(R.array.create))
            db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        switch (oldVersion) {
            case 1:
        }
    }

    // -------------------------------------------PERSON-------------------------------------------
    public int addpers(Person person) {

        long ins = 0;

        SQLiteDatabase database = this.getWritableDatabase();
        int dbv = database.getVersion();
        ContentValues values = new ContentValues();
        values.put(KEY_VNAME, person.getvname()); //
        values.put(KEY_NNAME, person.getnname()); //
        values.put(KEY_ADR, person.getadr()); //
        values.put(KEY_PLZ, person.getplz()); //
        values.put(KEY_ORT, person.getort()); //
        values.put(KEY_GEB, person.getgeb()); //
        values.put(KEY_TEL, person.gettel());
        values.put(KEY_PIC, person.getpic());//

        ins = database.insert(TABLE_PERS, null, values);

//        database.close(); // Closing database connection

        int ret = (int) ins;
        return ret;
    }

    public int updpers(Person person, String[] pid, SQLiteDatabase database) {

        long ins;

        ContentValues values = new ContentValues();
        values.put(KEY_VNAME, person.getvname()); //
        values.put(KEY_NNAME, person.getnname()); //
        values.put(KEY_ADR, person.getadr()); //
        values.put(KEY_PLZ, person.getplz()); //
        values.put(KEY_ORT, person.getort()); //
        values.put(KEY_GEB, person.getgeb()); //
        values.put(KEY_TEL, person.gettel()); //
        values.put(KEY_PIC, person.getpic());//

        ins = database.update(TABLE_PERS, values,
                KEY_PID + " =?",
                pid);

        database.close(); // Closing database connection
        return (int) ins;
    }


    public Person opensingleperson(int pid, SQLiteDatabase database) {

        Cursor cursor = database.query(TABLE_PERS, allColumns, KEY_PID + " =" + pid, null, null, null, null);
        Person pers1 = null;

        if (cursor.moveToFirst()) {

            pers1 = cursorToPerson(cursor);
        }

        database.close();

        return pers1;
    }


    public ArrayList<Person> getAllPerson() {
        ArrayList<Person> persons = new ArrayList<Person>();
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor cursor = database.query(TABLE_PERS, allColumns, null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Person person = cursorToPersonHeaderInfo(cursor);
            persons.add(person);
            cursor.moveToNext();

        }
        database.close();
        return persons;

    }

    private Person cursorToPersonHeaderInfo(Cursor cursor) {
        Person person = new Person();
        person.setID(cursor.getInt(0));
        person.setvname(cursor.getString(1));
        person.setnname(cursor.getString(2));
        person.setort(cursor.getString(3));

        return person;
    }

    private Person cursorToPerson(Cursor cursor) {

        Person person = new Person();
        person.setID(cursor.getInt(0));
        person.setvname(cursor.getString(1));
        person.setnname(cursor.getString(2));
        person.setadr(cursor.getString(3));
        person.setplz(cursor.getString(4));
        person.setort(cursor.getString(5));
        person.setgeb(cursor.getString(6));
        person.settel(cursor.getString(7));
        person.setpic(cursor.getString(8));
        return person;
    }

    public void deleteperson(int pid, SQLiteDatabase database) {

        database.delete(TABLE_PERS,
                KEY_PID + " = " + pid, null);

        database.delete(TABLE_CAND,
                KEY_CPER + " = " + pid, null);

        database.close();

        return;

    }


    // -------------------------------------------Training-------------------------------------------


    public int addtraining(Training training) {

        long ins = 0;

        SQLiteDatabase database = this.getWritableDatabase();
        int dbv = database.getVersion();
        ContentValues values = new ContentValues();
        values.put(KEY_WOTAG, training.getwotag()); //
        values.put(KEY_STIME, training.getstartzeit()); //
        values.put(KEY_ETIME, training.getendzeit()); //

        ins = database.insert(TABLE_TRAIN, null, values);

        database.close(); // Closing database connection
        return (int) ins;
    }


    public ArrayList<Training> getAllTrainings() {
        ArrayList<Training> trainings = new ArrayList<Training>();
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor cursor = database.query(TABLE_TRAIN, allColumns2, null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Training training = cursorToTrainingHeaderInfo(cursor);
            trainings.add(training);
            cursor.moveToNext();

        }
        database.close();
        return trainings;

    }

    private Training cursorToTrainingHeaderInfo(Cursor cursor) {
        Training training = new Training();
        training.setID(cursor.getInt(0));
        training.setwotag(cursor.getString(1));
        training.setstartzeit(cursor.getString(2));
        training.setendzeit(cursor.getString(3));

        return training;
    }

    public void deletetraining(int tid, SQLiteDatabase database) {

        database.delete(TABLE_TRAIN,
                KEY_TID + " = " + tid, null);

        database.delete(TABLE_CAND,
                KEY_CTRA + " = " + tid, null);

        database.close();

        return;

    }


//-------------------------------------------Participants--------------------------------------------------------------------
	    
    public int addparticipants(Participant participant) {

        long ins = 0;

        SQLiteDatabase database = this.getWritableDatabase();
        int dbv = database.getVersion();
        ContentValues values = new ContentValues();
        values.put(KEY_TTRAIN, participant.gettrain());
        values.put(KEY_DAT, participant.getdate());
        values.put(KEY_TREF, participant.gettref()); //
        values.put(KEY_PREF, participant.getpref()); //

        ins = database.insert(TABLE_PART, null, values);

        database.close(); // Closing database connection
        return (int) ins;
    }


    public List<Participant> getAllParticipants() {
        List<Participant> participants = new ArrayList<Participant>();
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor cursor = database.query(TABLE_PART, allColumns3, null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Participant participant = cursorToParticipantHeaderInfo(cursor);
            participants.add(participant);
            cursor.moveToNext();
        }

        return participants;

    }

    private Participant cursorToParticipantHeaderInfo(Cursor cursor) {
        Participant participant = new Participant();
        participant.setID(cursor.getInt(0));
        participant.settrain(cursor.getString(1));
        participant.setdate(cursor.getString(2));
        participant.settref(cursor.getString(3));
        participant.setpref(cursor.getString(4));

        return participant;
    }

//-------------------------------------------Candidates--------------------------------------------------------------------


    public int addcandidates(Candidate candidate) {

        long ins = 0;

        SQLiteDatabase database = this.getWritableDatabase();
        int dbv = database.getVersion();
        ContentValues values = new ContentValues();
        values.put(KEY_TREF, candidate.gettref()); //
        values.put(KEY_PREF, candidate.getpref()); //

        ins = database.insert(TABLE_CAND, null, values);

        database.close(); // Closing database connection
        return (int) ins;
    }

    public Candidate opensinglecandidate(int pid, int trid, SQLiteDatabase database) {

        Cursor cursor = database.query(TABLE_CAND, allcolumns4, KEY_CPER + " =" + (String.valueOf(pid))  + " AND " + KEY_CTRA + "=" + (String.valueOf(trid)), null, null, null, null);
        Candidate cand1 = null;

        if (cursor.moveToFirst()) {

            cand1 = cursorToCandidateHeaderInfo(cursor);
        }

        database.close();

        return cand1;
    }


    public List<Candidate> getAllCandidates() {
        List<Candidate> candidates = new ArrayList<Candidate>();
        SQLiteDatabase database = this.getReadableDatabase();
        Cursor cursor = database.query(TABLE_CAND, allcolumns4, null, null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Candidate candidate = cursorToCandidateHeaderInfo(cursor);
            candidates.add(candidate);
            cursor.moveToNext();
        }

        return candidates;

    }

    private Candidate cursorToCandidateHeaderInfo(Cursor cursor) {
        Candidate candidate = new Candidate();
        candidate.setID(cursor.getInt(0));
        candidate.settref(cursor.getString(1));
        candidate.setpref(cursor.getString(2));

        return candidate;
    }


}