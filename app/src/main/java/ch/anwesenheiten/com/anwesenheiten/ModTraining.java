package ch.anwesenheiten.com.anwesenheiten;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Node;

import java.util.ArrayList;
import java.util.List;


public class ModTraining extends Activity  implements AdapterView.OnItemSelectedListener {

    public int[] trefindex = new int[24];
    private EditText  estartzeit, eendzeit;
    private String s_wotag, s_startzeit, s_endzeit;
    private boolean cb;
    Bundle bundle;
    Spinner spinner;
    List<Training> values;
    ArrayList<Training> trainings;
    ListView lv;
    int nbrp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_training);


        //Initialize fields

        for (int i = 0; i < trefindex.length; i++) {
            trefindex[i] = 0;
        }

        estartzeit = (EditText) findViewById(R.id.startzeit);
        eendzeit = (EditText) findViewById(R.id.endzeit);

        spinner = (Spinner) findViewById(R.id.wotag_spinner);
        // Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.wotag_array, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(this);

        filltrainings();

    }

    public void save(View v) {
        AnwesenheitenDBHandler database = new AnwesenheitenDBHandler(this);
        SQLiteDatabase connection = database.getWritableDatabase();

        s_startzeit = estartzeit.getText().toString();
        s_endzeit = eendzeit.getText().toString();

        int tid = (int) database.addtraining(new Training(s_wotag, s_startzeit, s_endzeit,  cb));

        database.close();

        Toast.makeText(this, "Training saved", Toast.LENGTH_SHORT).show();;

        filltrainings();

    }

    @Override
    public View findViewById(int id) {
        return super.findViewById(id);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

        s_wotag = (String ) spinner.getItemAtPosition(i);
        String s = s_wotag;
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    public void filltrainings () {

        AnwesenheitenDBHandler database = new AnwesenheitenDBHandler(this);
        SQLiteDatabase connection = database.getWritableDatabase();

        trainings = database.getAllTrainings();

        int i1=0,i2=0,i3=0,i4=0,i5=0,i6=0;
        int ii = 0;
        int etid=0;
        String editTextID;
        int resID;
        EditText t;
        Training tr = new Training();

        for (int i = 0; i < trainings.size(); i++) {
            tr = trainings.get(i);
            String dow = tr.getwotag();
            switch (dow) {
                case "Montag":
                    ii=i1*6;
                    etid = ii+1;
                    editTextID = "editText" + etid;
                    resID = getResources().getIdentifier(editTextID, "id",
                            "ch.anwesenheiten.com.anwesenheiten");
                    t = (EditText) findViewById(resID);
                    t.setText(tr.getstartzeit()+" - "+tr.getendzeit());
                    trefindex[etid-1] = tr.getID();
                    i1++;

                    break;
                case "Dienstag":
                    ii=i2*6;
                    etid = ii+2;
                    editTextID = "editText" + etid;
                    resID = getResources().getIdentifier(editTextID, "id",
                            "ch.anwesenheiten.com.anwesenheiten");
                    t = (EditText) findViewById(resID);
                    t.setText(tr.getstartzeit()+" - "+tr.getendzeit());
                    trefindex[etid-1] = tr.getID();
                    i2++;
                    break;
                case "Mittwoch":
                    ii=i3*6;
                    etid = ii+3;
                    editTextID = "editText" + etid;
                    resID = getResources().getIdentifier(editTextID, "id",
                            "ch.anwesenheiten.com.anwesenheiten");
                    t = (EditText) findViewById(resID);
                    t.setText(tr.getstartzeit()+" - "+tr.getendzeit());
                    trefindex[etid-1] = tr.getID();
                    i3++;

                    break;
                case "Donnerstag":
                    ii=i4*6;
                    etid = ii+4;
                    editTextID = "editText" + etid;
                    resID = getResources().getIdentifier(editTextID, "id",
                            "ch.anwesenheiten.com.anwesenheiten");
                    t = (EditText) findViewById(resID);
                    t.setText(tr.getstartzeit()+" - "+tr.getendzeit());
                    trefindex[etid-1] = tr.getID();
                    i4++;

                    break;
                case "Freitag":
                    ii=i5*6;
                    etid = ii+4;
                    editTextID = "editText" + etid;
                    resID = getResources().getIdentifier(editTextID, "id",
                            "ch.anwesenheiten.com.anwesenheiten");
                    t = (EditText) findViewById(resID);
                    t.setText(tr.getstartzeit()+" - "+tr.getendzeit());
                    trefindex[etid-1] = tr.getID();
                    i5++;

                    break;
                case "Samstag":
                    ii=i6*6;
                    etid = ii+6;
                    editTextID = "editText" + etid;
                    resID = getResources().getIdentifier(editTextID, "id",
                            "ch.anwesenheiten.com.anwesenheiten");
                    t = (EditText) findViewById(resID);
                    t.setText(tr.getstartzeit()+" - "+tr.getendzeit());
                    trefindex[etid-1] = tr.getID();
                    i6++;

                    break;

            }
        }
    }

    public void ontrfieldclicked(View v) {

        int i = gettref(v);

        //retrieve DB ref of training

        final int trDBref = trefindex[i];

        //verify if delete is really wanted

        DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override


            public void onClick(DialogInterface dialog, int which) {
                switch (which){
                    case DialogInterface.BUTTON_POSITIVE:

                        deletetraining(trDBref);

                        break;

                    case DialogInterface.BUTTON_NEGATIVE:
                        //No button clicked
                        break;
                }
            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
        builder.setMessage("Delete Training?").setPositiveButton("Yes", dialogClickListener)
                .setNegativeButton("No", dialogClickListener).show();

        //execute delete with value found in tref index

    }

    public void deletetraining(int trid) {
        AnwesenheitenDBHandler database = new AnwesenheitenDBHandler(this);
        SQLiteDatabase connection = database.getWritableDatabase();

        database.deletetraining(trid, connection);

        Toast.makeText(this, "Training gelöscht", Toast.LENGTH_SHORT).show();;

        clearall();
        filltrainings();

    }

    public void clearall() {

        for (int i = 1; i < 25; i++) {

            String editTextID = "editText" + i;
            int resID = getResources().getIdentifier(editTextID, "id",
                    "ch.anwesenheiten.com.anwesenheiten");
            EditText et = (EditText) findViewById(resID);

            et.setText("");
         }
    };

    public int gettref(View v) {


            int i = 0;

            switch (v.getId())  //get the id of the view clicked. (in this case button)
            {
                case R.id.editText1: // if its eT1
                    i = 0;
                    break;
                case R.id.editText2: // if its button1
                    i= 1;
                    break;
                case R.id.editText3: // if its button2
                    i= 2;
                    break;
                case R.id.editText4: // if its button3
                    i= 3;
                    break;
                case R.id.editText5: // if its button4
                    i= 4;
                    break;
                case R.id.editText6: // if its button5
                    i= 5;
                    break;
                case R.id.editText7: // if its button6
                    i= 6;
                    break;
                case R.id.editText8: // if its button7
                    i= 7;
                    break;
                case R.id.editText9: // if its button8
                    i= 8;
                    break;
                case R.id.editText10: // if its button9
                    i= 9;
                    break;
                case R.id.editText11: // if its button10
                    i= 10;
                    break;
                case R.id.editText12: // if its button11
                    i= 11;
                    break;
                case R.id.editText13: // if its button12
                    i= 12;
                    break;
                case R.id.editText14: // if its button13
                    i= 13;
                    break;
                case R.id.editText15: // if its button14
                    i= 14;
                    break;
                case R.id.editText16: // if its button15
                    i= 15;
                    break;
                case R.id.editText17: // if its button16
                    i= 16;
                    break;
                case R.id.editText18: // if its button17
                    i= 17;
                    break;
                case R.id.editText19: // if its button18
                    i= 18;
                    break;
                case R.id.editText20: // if its button19
                    i= 19;
                    break;
                case R.id.editText21: // if its button20
                    i= 20;
                    break;
                case R.id.editText22: // if its button21
                    i= 21;
                    break;
                case R.id.editText23: // if its button22
                    i= 22;
                    break;
                case R.id.editText24: // if its button23
                    i= 23;
                    break;

            }
            return i;
    }
}