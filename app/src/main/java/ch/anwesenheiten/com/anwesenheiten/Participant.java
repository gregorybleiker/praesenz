package ch.anwesenheiten.com.anwesenheiten;

public class Participant {

    //private variables
    int _id;
    String _C_train;
    String _C_date;
    String _C_tref;
    String _C_pref;

    private String participant;

    // Empty constructor
    public Participant(){

    }
    // constructor
    public Participant(String train, String date, String tref, String pref){
        //this._id = _id;
        this._C_train = train;
        this._C_date = date;
        this._C_tref = tref;
        this._C_pref = pref;

    }
 

    // getting ID
    public int getID(){
        return this._id;
    }
 
    // setting id
    public void setID(int id){
        this._id = id;
    }

    // getting date
    public String gettrain(){
        return this._C_train;
    }

    // setting date
    public void settrain(String train){
        this._C_train = train;
    }

    // getting date
    public String getdate(){
        return this._C_date;
    }

    // setting date
    public void setdate(String date){
        this._C_date = date;
    }

    // getting training ref
    public String gettref(){
        return this._C_tref;
    }
 
    // setting training ref
    public void settref(String tref) {this._C_tref = tref; }
 
    // getting person ref
    public String getpref(){
        return this._C_pref;
    }
 
    // setting person ref
    public void setpref(String pref){
        this._C_pref = pref;
    }
 

/*
    // Will be used by the ArrayAdapter in the ListView
    @Override
    public String toString() {
      return String.format("%s %s %s", _T_tname, _T_sdate, _T_edate );
      
    }
*/
    public String alltoString() {
      return String.format("%s,%s;%s;%s \n", _C_train,_C_date, _C_tref, _C_pref);
    	
	}
}
