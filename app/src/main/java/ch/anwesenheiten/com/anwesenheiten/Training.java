package ch.anwesenheiten.com.anwesenheiten;

public class Training {

    //private variables
    int _id;
    String _T_wotag;
    String _T_startzeit;
    String _T_endzeit;
    boolean _T_cb;

    private String training;

    // Empty constructor
    public Training(){

    }
    // constructor
    public Training(String wotag, String startzeit, String endzeit, boolean cb ){

        this._T_wotag = wotag;
        this._T_startzeit = startzeit;
        this._T_endzeit = endzeit;
        this._T_cb = cb;
    }
 

    // getting ID
    public int getID(){
        return this._id;
    }
 
    // setting id
    public void setID(int id){
        this._id = id;
    }
 
    // getting wotag
    public String getwotag() {return this._T_wotag ;    }
 
    // setting wotag
    public void setwotag(String wotag) {this._T_wotag = wotag; }
 
    // getting startzeit
    public String getstartzeit () {return this._T_startzeit;}
 
    // setting startzeit
    public void setstartzeit(String startzeit){
        this._T_startzeit = startzeit;
    }
 
    // getting endzeit
    public String getendzeit(){
        return this._T_endzeit;
    }
 
    // setting endzeit
    public void setendzeit(String endzeit){this._T_endzeit = endzeit;}

    // getting cb
    public boolean getcb() {return this._T_cb; }

    // setting cb
    public void setcb(boolean cb) {this._T_cb = cb;}
 
    public String alltoString() {
      return String.format("%s;%s;%s \n", _T_wotag, _T_startzeit);
    	
	}

    public void count() {
    }
}
