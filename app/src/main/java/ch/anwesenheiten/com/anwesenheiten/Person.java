package ch.anwesenheiten.com.anwesenheiten;

public class Person {
	 
    //private variables
    int _id;
    String _T_vname;
    String _T_nname;
    String _T_adr;
    String _T_plz;
    String _T_ort;
    String _T_geb;
    String _T_tel;
    String _T_pic;
    private String person;
    
    // Empty constructor
    public Person(){
 
    }
    // constructor
    public Person(String vname, String nname, String adr, String plz, String ort, String geb, String tel, String pic ){
        //this._id = _id;
        this._T_vname = vname;
        this._T_nname = nname;
        this._T_adr = adr;
        this._T_plz = plz;
        this._T_ort = ort;
        this._T_geb = geb;
        this._T_tel = tel;
        this._T_pic = pic;
    }
 

    // getting ID
    public int getID(){
        return this._id;
    }
 
    // setting id
    public void setID(int id){
        this._id = id;
    }
 
    // getting vname
    public String getvname(){
        return this._T_vname;
    }
 
    // setting vname
    public void setvname(String vname) {this._T_vname = vname; }
 
    // getting nname
    public String getnname(){
        return this._T_nname;
    }
 
    // setting nname
    public void setnname(String nname){
        this._T_nname = nname;
    }
 
    // getting adr
    public String getadr(){
        return this._T_adr;
    }
 
    // setting adr
    public void setadr(String adr){this._T_adr = adr;}
 
    // getting plz
    public String getplz(){
        return this._T_plz;
    }
 
    // setting plz
    public void setplz(String plz){
        this._T_plz = plz;
    }
    
    // getting ort
    public String getort(){
        return this._T_ort;
    }
 
    // setting ort
    public void setort (String ort)  {this._T_ort = ort;
    } 
    // getting geb
    public String getgeb(){
        return this._T_geb;
    }
 
    // setting geb
    public void setgeb (String geb){
        this._T_geb = geb;
    }
 
    // getting tel
    public String gettel (){return this._T_tel; }
 
    // setting tel
    public void settel(String tel){this._T_tel = tel; }

    // getting pic
    public String getpic (){return this._T_pic; }

    // setting pic
    public void setpic(String pic){this._T_pic = pic; }

/*
    // Will be used by the ArrayAdapter in the ListView
    @Override
    public String toString() {
      return String.format("%s %s %s", _T_tname, _T_sdate, _T_edate );
      
    }
*/
    public String alltoString() {
      return String.format("%s;%s;%s;%s;%s;%s \n", _T_vname, _T_nname, _T_adr, _T_plz, _T_ort, _T_geb, _T_tel);
    	
	}
}
