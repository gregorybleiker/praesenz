package ch.anwesenheiten.com.anwesenheiten;

/**
 * Created by georg on 25.03.18.
 */
import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;


public class SelectTrainings extends Activity {
            MyCustomAdapter dataAdapter = null;
            int nbrp;
            String pref;

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_sel_training);

            Bundle bundle = getIntent().getExtras();
            nbrp = bundle.getInt("nbrpers");
            pref= Integer.toString(nbrp);

            //Generate list View from ArrayList
            displayListView();

            checkButtonClick();

        }

        private void displayListView() {

            //Array list of countries
            ArrayList<Training> trainings = new ArrayList<Training>();
            AnwesenheitenDBHandler database = new AnwesenheitenDBHandler(this);
            trainings = database.getAllTrainings();

            //create an ArrayAdaptar from the String Array
            dataAdapter = new MyCustomAdapter(this,
                    R.layout.seltrain_list, trainings);
            ListView lv = (ListView) findViewById(R.id.listview);
            // Assign adapter to ListView
            lv.setAdapter(dataAdapter);


            lv.setOnItemClickListener(new OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {
                    // When clicked, show a toast with the TextView text
                    Training trainings = (Training) parent.getItemAtPosition(position);
                    Toast.makeText(getApplicationContext(),
                            "Clicked on Row: " + trainings.getwotag(),
                            Toast.LENGTH_LONG).show();
                }
            });

        }

        private class MyCustomAdapter extends ArrayAdapter<Training> {

            private ArrayList<Training> trainings;

            public MyCustomAdapter(Context context, int textViewResourceId,
                                   ArrayList<Training> trainings) {
                super(context, textViewResourceId, trainings);
                this.trainings = new ArrayList<Training>();
                this.trainings.addAll(trainings);
            }

            private class ViewHolder {
                TextView id;
                TextView wotag;
                TextView stime;
                TextView etime;
                CheckBox cb;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {

                ViewHolder holder = null;
                Log.v("ConvertView", String.valueOf(position));

                if (convertView == null) {
                    LayoutInflater vi = (LayoutInflater)getSystemService(
                            Context.LAYOUT_INFLATER_SERVICE);
                    convertView = vi.inflate(R.layout.seltrain_list, null);

                    holder = new ViewHolder();
                    holder.id = (TextView) convertView.findViewById(R.id.tid);
                    holder.wotag = (TextView) convertView.findViewById(R.id.label1);
                    holder.stime = (TextView) convertView.findViewById(R.id.label2);
                    holder.etime = (TextView) convertView.findViewById(R.id.label3);
                    holder.cb = (CheckBox) convertView.findViewById(R.id.checkBox);
                    convertView.setTag(holder);

                    holder.cb.setOnClickListener( new View.OnClickListener() {
                        public void onClick(View v) {
                            CheckBox cb = (CheckBox) v ;
                            Training tr = (Training) cb.getTag();
                            Toast.makeText(getApplicationContext(),
                                    "Clicked on Checkbox: " + cb.getText() +
                                            " is " + cb.isChecked(),
                                    Toast.LENGTH_LONG).show();
                            tr.setcb(cb.isChecked());
                        }
                    });
                }
                else {
                    holder = (ViewHolder) convertView.getTag();
                }

                Training training = trainings.get(position);
                holder.id.setText(Integer.toString(training.getID()));
                holder.wotag.setText(training.getwotag());
                holder.stime.setText(training.getstartzeit());
                holder.etime.setText(training.getendzeit());
                holder.cb.setText(training.getwotag());

                int persid = nbrp;
                int trainid = training.getID();

                AnwesenheitenDBHandler database = new AnwesenheitenDBHandler(getContext());
                SQLiteDatabase connection = database.getWritableDatabase();
                Candidate cand = database.opensinglecandidate(persid, trainid, connection);

                if (cand != null) {

                    holder.cb.setChecked(true);
                }
                holder.cb.setTag(training);

                return convertView;

            }

        }

        private void checkButtonClick() {


            Button myButton = (Button) findViewById(R.id.savebutton);
            myButton.setOnClickListener(new OnClickListener() {

                @Override
                public void onClick(View v) {

                    AnwesenheitenDBHandler database = new AnwesenheitenDBHandler(getBaseContext());
                    SQLiteDatabase connection = database.getWritableDatabase();



                    StringBuffer responseText = new StringBuffer();
                    responseText.append("The following were selected...\n");

                    ArrayList<Training> trainings = dataAdapter.trainings;
                    for(int i=0;i<trainings.size();i++){
                        Training training = trainings.get(i);
                        if(training.getcb()){
                            responseText.append("\n" + training.getwotag());
                            String stref = Integer.toString(training.getID());
                            int pid = (int) database.addcandidates(new Candidate(stref, pref));
                        }
                    }
                    database.close();
                    Toast.makeText(getApplicationContext(),
                            responseText, Toast.LENGTH_LONG).show();
                    finish();
                }
            });

        }



    }
