package ch.anwesenheiten.com.anwesenheiten;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.app.Activity;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import java.text.SimpleDateFormat;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;



public class ModPerson extends Activity {
    private static final int REQUEST_TAKE_PHOTO = 1;
    private EditText evname, enname, eadr, eplz, eort, egeb, etel;
    private String s_vname, s_nname, s_adr, s_plz, s_ort, s_geb, s_tel, s_pic;
    Boolean flagpic = false;
    Bundle bundle;
    Person pers;
    ArrayList<Training> values;
    private String[] strid = new String[1];
    String dir = "";
    File newfile;
    int TAKE_PHOTO_CODE = 0;
    int nbrp=0;
    String mCurrentPhotoPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_person);

        dir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/Anwesenheiten/picFolder/";
        File newdir = new File(dir);
        newdir.mkdirs();

        bundle = getIntent().getExtras();
        nbrp = bundle.getInt("nbrpers");

        evname = (EditText) findViewById(R.id.vname);
        enname = (EditText) findViewById(R.id.nname);
        eadr = (EditText) findViewById(R.id.adresse);
        eplz = (EditText) findViewById(R.id.plz);
        eort = (EditText) findViewById(R.id.ort);
        egeb = (EditText) findViewById(R.id.geb);
        etel = (EditText) findViewById(R.id.tel);

        AnwesenheitenDBHandler database = new AnwesenheitenDBHandler(this);
        SQLiteDatabase connection = database.getWritableDatabase();

        pers = database.opensingleperson(nbrp, connection);

        if (pers != null) {
//            pid = pers.getID();
            evname.setText(s_vname = pers.getvname());
            enname.setText(s_nname = pers.getnname());
            eadr.setText(s_adr = pers.getadr());
            eplz.setText(s_plz = pers.getplz());
            eort.setText(s_ort = pers.getort());
            egeb.setText(s_geb = pers.getgeb());
            etel.setText(s_tel = pers.gettel());

            ImageView myImage = (ImageView) findViewById(R.id.imageView1);

            Bitmap myBitmap = BitmapFactory.decodeFile(s_pic = pers.getpic());

            if (myBitmap != null) {
                Matrix matrix = new Matrix();
                matrix.postRotate(00);
                Bitmap rotatedBitmap = Bitmap.createBitmap(myBitmap, 0, 0, myBitmap.getWidth(), myBitmap.getHeight(), matrix, true);

                myImage.setImageBitmap(rotatedBitmap);
            }
        }

            database.close();

    }

    public void save(View v) {
        AnwesenheitenDBHandler database = new AnwesenheitenDBHandler(this);
        SQLiteDatabase connection = database.getWritableDatabase();

        s_vname = evname.getText().toString();


        s_nname = enname.getText().toString();


        s_adr = eadr.getText().toString();
        s_plz = eplz.getText().toString();
        s_ort = eort.getText().toString();
        s_geb = egeb.getText().toString();
        s_tel = etel.getText().toString();

        if (flagpic == false) {
                 s_pic = "people_mini.png";
        }

        if (pers==null) {
            pers = new Person(s_vname, s_nname, s_adr, s_plz, s_ort, s_geb, s_tel, s_pic);
            nbrp = (int) database.addpers(pers);
            }
        else   {
            strid[0] = String.valueOf(nbrp);
            pers.setvname(s_vname);
            pers.setnname(s_nname);
            pers.setadr(s_adr);
            pers.setplz(s_plz);
            pers.setort(s_ort);
            pers.setgeb(s_geb);
            pers.settel(s_tel);
            pers.setpic(s_pic);

            int pid = (int) database.updpers(pers, strid, connection);
        }
        database.close();

        Toast.makeText(this, "Person saved", Toast.LENGTH_LONG).show();;

    }

    public void delperson(View v) {

        if (pers!=null) {

            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override


                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case DialogInterface.BUTTON_POSITIVE:

                            delper();

                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            //No button clicked
                            break;
                    }
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
            builder.setMessage("delete Person?").setPositiveButton("Yes", dialogClickListener)
                    .setNegativeButton("No", dialogClickListener).show();

        }
        else
        {
            Toast.makeText(this, "No Person to delete found", Toast.LENGTH_LONG).show();

        }
    }

     public void delper() {

        AnwesenheitenDBHandler database = new AnwesenheitenDBHandler(this);
        SQLiteDatabase connection = database.getWritableDatabase();

        int pid = pers.getID();

        database.deleteperson(pid, connection);

        database.close();

        Toast.makeText(this, "Person deleted", Toast.LENGTH_LONG).show();

        finish();

    }

    public void getpic(View v) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
        String currentDateandTime = sdf.format(new Date());
        s_pic = dir+currentDateandTime+".jpg";
/*        newfile = new File(s_pic);
        try {
            newfile.createNewFile();
        }
        catch (IOException e)
        {
        }

        Uri outputFileUri = Uri.fromFile(newfile);
*/
        Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
 //       cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);


        if (cameraIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            //do nothing
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "ch.anwesenheiten.com.anwesenheiten",
                        photoFile);
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(cameraIntent, TAKE_PHOTO_CODE);
            }
        }

 //       startActivityForResult(cameraIntent, TAKE_PHOTO_CODE);

    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        s_pic = image.getAbsolutePath();

        return image;
    }

    public void seltrain(View v) {

        if (nbrp == 0) {
            save(v);
        }

        Intent intent = new Intent(this, SelectTrainings.class);
        intent.putExtra("nbrpers", nbrp);
        startActivity(intent);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == TAKE_PHOTO_CODE && resultCode == RESULT_OK) {

            try {
                newfile = new File(s_pic);

                    newfile.createNewFile();
                    flagpic = true;
                } catch (IOException e) {
                }
            }

                Bitmap myBitmap = BitmapFactory.decodeFile(newfile.getAbsolutePath());

                ImageView myImage = (ImageView) findViewById(R.id.imageView1);

                Matrix matrix = new Matrix();
                matrix.postRotate(00);
                Bitmap rotatedBitmap = Bitmap.createBitmap(myBitmap , 0, 0, myBitmap .getWidth(), myBitmap .getHeight(), matrix, true);

                myImage.setImageBitmap(rotatedBitmap);

        }

    @Override
    public View findViewById(int id) {
        return super.findViewById(id);
    }

    public void onSaveInstanceState(Bundle savedInstanceState) {

        // Save UI state changes to the savedInstanceState.
        // This bundle will be passed to onCreate if the process is
        // killed and restarted.

        savedInstanceState.putBoolean("MyBoolean", true);
        savedInstanceState.putDouble("myDouble", 1.9);
        savedInstanceState.putInt("MyInt", 1);
        savedInstanceState.putString("MyString", s_pic);

        // etc.

        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {

        super.onRestoreInstanceState(savedInstanceState);

        // Restore UI state from the savedInstanceState.
        // This bundle has also been passed to onCreate.

        boolean myBoolean = savedInstanceState.getBoolean("MyBoolean");
        double myDouble = savedInstanceState.getDouble("myDouble");
        int myInt = savedInstanceState.getInt("MyInt");
        s_pic = savedInstanceState.getString("MyString");
    }
}